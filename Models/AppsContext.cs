using Microsoft.EntityFrameworkCore;

namespace LearCore.Models
{
    public class AppsContext : DbContext
    {
        public DbSet<tbl_error_log> tbl_Error_Logs { get; set; }
    }
}