﻿using System;
using System.Collections.Generic;

namespace LearCore.Models
{
    public partial class TblEmployee
    {
        public string Nrp { get; set; }
        public string Nama { get; set; }
    }
}
